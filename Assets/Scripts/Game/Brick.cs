﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{

	public float Health = 70f;
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

		float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;

		if (damage >= 10)
		{
			GetComponent<AudioSource>().Play();
			Health -= damage;
			var instanceCoin = Instantiate(GameManager.coin, transform.position, Quaternion.identity);
			
			GameManager.score += (int)damage;
		}
		if (Health <= 0)
		{
			GameManager.score += 100;
			var instanceCoin = Instantiate(GameManager.coin, transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
	}

}
