using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GreenBird : Bird
{
	[SerializeField] private GameObject greenBirdChild;
	[SerializeField] private CameraFollow cameraFollow;
	private bool instanceOneTime = true;
	private void Start()
	{

		cameraFollow = Camera.main.gameObject.GetComponent<CameraFollow>();
	}
	private void Update()
	{

		if (State == BirdState.Thrown)
		{
			if (Input.GetMouseButton(0))
			{
				DivideBird();
			}
		}
	}

	private void DivideBird()
	{
		if (instanceOneTime)
		{
			for (int i = 0; i < 2; i++)
			{
				var instanceChild = Instantiate(greenBirdChild, this.transform.position, Quaternion.identity);
				instanceChild.gameObject.GetComponent<Rigidbody2D>().velocity = (this.gameObject.GetComponent<Rigidbody2D>().velocity + Vector2.up * i * 1.5f);
				instanceChild.GetComponent<TrailRenderer>().enabled = true;
				instanceChild.GetComponent<CircleCollider2D>().radius = Constants.BirdColliderRadiusNormal;
			}
			instanceOneTime = false;
		}
	}
}
