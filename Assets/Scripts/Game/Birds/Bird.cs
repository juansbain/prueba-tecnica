﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
public class Bird : MonoBehaviour
{
	public BirdState State { get;  set; }
	public List<GameObject> enemys = new List<GameObject>();
	void Start()
	{
		var bricks = GameObject.FindGameObjectsWithTag("Brick");
		var pigs = GameObject.FindGameObjectsWithTag("Pig");
		foreach (var b in bricks)
		{
			enemys.Add(b);
		}
		foreach (var p in pigs)
		{
			enemys.Add(p);
		}
		GetComponent<TrailRenderer>().enabled = false;
		GetComponent<TrailRenderer>().sortingLayerName = "Foreground";
		GetComponent<Rigidbody2D>().isKinematic = true;
		GetComponent<CircleCollider2D>().radius = Constants.Bird_Collider_Radius_Big;
		State = BirdState.BeforeThrown;
	}

	
	void FixedUpdate()
	{
		if (State == BirdState.Thrown && GetComponent<Rigidbody2D>().velocity.sqrMagnitude <= Constants.Min_Velocity)
			StartCoroutine(DestroyAfter(2));
	}

	public void OnBirdShot()
	{
		GetComponent<AudioSource>().Play(); GetComponent<TrailRenderer>().enabled = true; GetComponent<Rigidbody2D>().isKinematic = false; GetComponent<CircleCollider2D>().radius = Constants.BirdColliderRadiusNormal; State = BirdState.Thrown;
	}

	IEnumerator DestroyAfter(float seconds) { yield return new WaitForSeconds(seconds); Destroy(gameObject); }






}
