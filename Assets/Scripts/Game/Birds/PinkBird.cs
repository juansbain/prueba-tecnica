using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkBird : Bird
{
	private bool explodeOneTime = true;
	private void Start()
	{
		explodeOneTime= true;
	}
	private void Update()
	{
		if (State == BirdState.Thrown)
		{
			if (Input.GetMouseButton(0))
			{
				ExplodeBird();
			}
		}
	}
	private void ExplodeBird()
	{
		if (explodeOneTime)
		{
			foreach (var enemy in enemys)
			{
				if (enemy != null)
				{
					var distance = Vector3.Distance(enemy.transform.position, transform.position);
					if (distance <= 10)
					{
						var force = new Vector3(100, 100, 0) - ((transform.position - enemy.transform.position)*10);
						Debug.Log(force);
						enemy.GetComponent<Rigidbody2D>().AddRelativeForce(force);
					}
				}
			}
			explodeOneTime = false;
		}
	}
}
