using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	private Vector3 endPosition;
	[SerializeField] private float speed;
	private void Start()
	{
		endPosition = transform.position + (Vector3.up * 2);
	}
	void Update()
	{
		float step = speed * Time.deltaTime;
		this.transform.position = Vector3.MoveTowards(transform.position, endPosition, step);
		Destroy(this.gameObject, 0.3f);
	}
}
