﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

	public CameraFollow cameraFollow;
	[HideInInspector]
	public static GameState CurrentGameState = GameState.Start;
	[HideInInspector]
	public static int score = 0;
	[HideInInspector]
	public static GameObject coin;
	[SerializeField]
	private StartScreenController startScreen;
	[SerializeField]
	private SlingShot slingshot;
	[SerializeField] private GameObject[] spawnPoints;
	[SerializeField] private EndScreen endScreen;
	[SerializeField] private ScoreController scoreController;
	[HideInInspector]
	private int currentBirdIndex;
	private List<GameObject> Bricks;
	private List<GameObject> Birds;
	private List<GameObject> Pigs;
	private List<GameObject> selectedBirds;
	

	private void Start()
	{
		score = 0;
		coin = (GameObject)Resources.Load("Coin");
		CurrentGameState = GameState.Selection;
		slingshot.enabled = false;
		Bricks = new List<GameObject>(GameObject.FindGameObjectsWithTag("Brick"));
		Pigs = new List<GameObject>(GameObject.FindGameObjectsWithTag("Pig"));
		slingshot.BirdThrown += SlingshotBirdThrown;
		startScreen.Play += StartGame;
		endScreen.Replay += RestartScene;
	}

	private void OnDestroy()
	{
		slingshot.BirdThrown -= SlingshotBirdThrown;
		startScreen.Play -= StartGame;
		endScreen.Replay -= RestartScene;
	}


	private void Update()
	{
		scoreController.UpdateScore(score);
		switch (CurrentGameState)
		{

			case GameState.Selection:
				startScreen.gameObject.SetActive(true);
				break;

			case GameState.Start:
				AnimateBirdToSlingshot();
				break;

			case GameState.BirdMovingToSlingshot:
				break;

			case GameState.Playing:
				if (slingshot.slingshotState == SlingshotState.BirdFlying &&
					(BricksBirdsPigsStoppedMoving() || Time.time - slingshot.TimeSinceThrown > 5f))
				{
					slingshot.enabled = false;
					AnimateCameraToStartPosition();
					CurrentGameState = GameState.BirdMovingToSlingshot;
				}
				break;

			case GameState.Won:
				endScreen.gameObject.SetActive(true);
				endScreen.UpdateText(true);
				break;

			case GameState.Lost:
				endScreen.gameObject.SetActive(true);
				endScreen.UpdateText(false);
				break;

			default:
				break;
		}
	}

	private void RestartScene()
	{
		SceneManager.LoadScene("Game");
	}
	private void StartGame(List<GameObject> selectedBirds)
	{
		Birds = new List<GameObject>();
		for (int i = 0; i < selectedBirds.Count; i++)
		{
			var birdInstantiate = Instantiate(selectedBirds[i], spawnPoints[i].transform.position, Quaternion.identity);
			Birds.Add(birdInstantiate);
		}
		this.selectedBirds = selectedBirds;
		startScreen.gameObject.SetActive(false);
		CurrentGameState = GameState.Start;
	}
	private bool AllPigsDestroyed()
	{
		return Pigs.All(x => x == null);
	}

	private void AnimateCameraToStartPosition()
	{
		float duration = Vector2.Distance(Camera.main.transform.position, cameraFollow.StartingPosition) / 10f;
		if (duration == 0.0f) duration = 0.1f;
		//animate the camera to start
		Camera.main.transform.positionTo
			(duration,
			cameraFollow.StartingPosition). //end position
			setOnCompleteHandler((x) =>
			{
				cameraFollow.IsFollowing = false;
				if (AllPigsDestroyed())
					CurrentGameState = GameState.Won;
				else if (currentBirdIndex == Birds.Count - 1)
					CurrentGameState = GameState.Lost;
				else
				{
					slingshot.slingshotState = SlingshotState.Idle;
					currentBirdIndex++;
					AnimateBirdToSlingshot();
				}
			});
	}

	private void AnimateBirdToSlingshot()
	{
		CurrentGameState = GameState.BirdMovingToSlingshot;
		Birds[currentBirdIndex].transform.positionTo
			(Vector2.Distance(Birds[currentBirdIndex].transform.position / 10,
			slingshot.BirdWaitPosition.transform.position) / 10, //duration
			slingshot.BirdWaitPosition.transform.position). //final position
				setOnCompleteHandler((x) =>
						{
							x.complete();
							x.destroy();
							CurrentGameState = GameState.Playing;
							slingshot.enabled = true;
							slingshot.BirdToThrow = Birds[currentBirdIndex];
						});
	}

	private void SlingshotBirdThrown(object sender, System.EventArgs e)
	{
		cameraFollow.BirdToFollow = Birds[currentBirdIndex].transform;
		cameraFollow.IsFollowing = true;
	}

	bool BricksBirdsPigsStoppedMoving()
	{
		foreach (var item in Bricks.Union(Birds).Union(Pigs))
		{
			if (item != null && item.GetComponent<Rigidbody2D>().velocity.sqrMagnitude > Constants.Min_Velocity)
			{
				return false;
			}
		}

		return true;
	}



}
