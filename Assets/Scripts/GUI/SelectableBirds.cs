using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class SelectableBirds : MonoBehaviour
{

	public List<GameObject> selectedBirds { get; private set; }
	public List<GameObject> defaultSelectedBirds { get; private set; }
	[SerializeField] private GameObject[] selectableBirds;
	[SerializeField] private GameObject selectables;
	[SerializeField] private GameObject selections;
	[SerializeField] private Sprite defaultSelection;
	[SerializeField] private int numberOfBirds = 3;
	[SerializeField] private Button finishSelection;
	[SerializeField] private Text alert;
	[SerializeField] private GameObject initialScreen;
	private GameObject[] selectionsImages;
	private GameObject birdSelection;


	private void Awake()
	{
		birdSelection = (GameObject)Resources.Load("BirdSelection");

	}

	private void Start()
	{
		selectedBirds = new List<GameObject>();
		defaultSelectedBirds = new List<GameObject>();
		selectionsImages = new GameObject[numberOfBirds];
		foreach (GameObject selectable in selectableBirds)
		{
			var bird = Instantiate(birdSelection, selectables.transform);
			bird.name = selectables.name;
			bird.gameObject.GetComponent<Image>().sprite = selectable.GetComponent<SpriteRenderer>().sprite;
			bird.gameObject.GetComponent<Button>().onClick.AddListener(delegate
			{
				CheckBirdOnSelection(selectable, bird);

			});
		}
		for (int i = 0; i < numberOfBirds; i++)
		{
			defaultSelectedBirds.Add(selectableBirds[i]);
			var selection = Instantiate(birdSelection, selections.transform);
			selection.name = selectables.name;
			selectionsImages[i] = selection;
			selection.gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(64, 28, 10, 255);
		}
		finishSelection.onClick.AddListener(delegate
		{
			CheckFinishSelection();
		});
	}

	private void CheckFinishSelection()
	{
		if (selectedBirds.Count() != numberOfBirds)
		{
			alert.gameObject.SetActive(true);
			alert.text = $"You must select {numberOfBirds} birds";
		}
		else
		{
			alert.gameObject.SetActive(false);
			initialScreen.SetActive(true);
			this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
		}
	}

	private void CheckBirdOnSelection(GameObject selectable, GameObject bird)
	{

		if (!selectedBirds.Contains(selectable) && selectedBirds.Count <= numberOfBirds - 1)
		{
			selectedBirds.Add(selectable);
			bird.gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(17, 255, 0, 255);
		}
		else if (selectedBirds.Contains(selectable))
		{
			selectedBirds.Remove(selectable);
			bird.gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(253, 255, 0, 255);
		}

		UpdateSelectables();

	}

	private void UpdateSelectables()
	{
		int i = 0;
		foreach (var selectable in selectedBirds)
		{
			selectionsImages[i].GetComponent<Image>().sprite = selectable.GetComponent<SpriteRenderer>().sprite;
			i++;
		}
		for (int j = i; j < numberOfBirds; j++)
		{
			selectionsImages[i].GetComponent<Image>().sprite = defaultSelection;
		}
	}

}
