using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
	[SerializeField] private Text score;
	private void Start()
	{
		score.text= $"Score:{0}";
	}
	public void UpdateScore(int newScore)
	{
		score.text = $"Score:{newScore}";
	}
}
