using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    [SerializeField] private Button replayButton;
	[SerializeField] private Text alertText;
	public Action Replay;
	private void Start()
	{
		replayButton.onClick.AddListener(delegate { Replay(); });
	}
	public void UpdateText(bool isWon )
	{
		if(isWon)
		{
			alertText.text = "You've won";
			alertText.color = new Color32(24, 255, 0, 255);
		}
		else
		{

			alertText.text = "You've lost";
			alertText.color = new Color32(255, 0, 18, 255);
		}
	}
}
