using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class StartScreenController : MonoBehaviour
{
	[SerializeField]
	private Button play;
	[SerializeField]
	private SelectableBirds selectableBirds;
    private List<Sprite> selectedBirds;
	public Action<List<GameObject>> Play;
	private void Start()
	{
		
		play.onClick.AddListener(delegate
		{
			if (selectableBirds.selectedBirds.Count != 0)
			{
				Play(selectableBirds.selectedBirds);
			}
			else
			{
				Play(selectableBirds.defaultSelectedBirds);
			}

		});
	}
}
