﻿using UnityEngine;

public class ParallaxScrolling : MonoBehaviour
{


	private Camera camera;
	public float ParallaxFactor;
	private Vector3 previous_CameraTransform;

	private void Start()
	{
		camera = Camera.main;
		previous_CameraTransform = camera.transform.position;
	}

	private void Update()
	{
		Vector3 delta = camera.transform.position - previous_CameraTransform;
		delta.y = 0; delta.z = 0;
		transform.position += delta / ParallaxFactor;
		previous_CameraTransform = camera.transform.position;
	}


}
